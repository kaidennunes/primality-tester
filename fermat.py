#Kaiden Nunes
#CS 312, Section 1

import math
import random

# O(n^4) time complexity
# O(n^2) space complexity
def prime_test(N, k):
	# Handle base cases. 
	# The time complexity of this is 3n, where n is the number of bits in N (we must compare every individual bit). So O(n), although hardware could probably do it in constant time.
	if N == 0 or N == 1:
		return 'composite'
	if N == 2:
		return 'prime'
	
	# We start the range from 2 because 1 to the power of anything will always be 1, which, for modded by any N greater than 1, will always be equal to 1. Not a useful test
	# This appears to use a rolling dice method, randomly choosing numbers from the pool. So, this is k*n time complexity, probably. Without source code, it is difficult to know. Also, we don't know the complexity python uses to get a random number.
	# However, because k is the number of tests we will run, it will certainly be a relatively small number, so I will assume it to be constant and not include it in any big O conclusions. So, O(n) for this line in time complexity
	# Space complexity is O(n), where n is the number of bits in N. I choose to use n because I already measure time complexity with n. Also, k is assumed to be constant multiplication to the complexity, so it doesn't figure into the space complexity here.
	testIntegers = random.sample(range(2, N), k)
	
	# If N fails the Fermat test for any of the test integers, it is a composite number. If it doesn't, we must check to see if it is a Carmichael number
	# The loop will run k times. Since k is, as mentioned before, assumed to be very relatively small, and so constant in complexity, this loop won't be used in the big O equation
	for testInteger in testIntegers:
		# O(n^3) time complexity, as calculated in the function
		# O(n^2) space complexity, as calculated in the function
		if mod_exp(testInteger, N - 1, N) != 1:
			return 'composite'
		# Check for Carmichael numbers
		# O(n^4) time complexity, as calculated in the function
		# O(n^2) space complexity, as calculated in the function
		if is_carmichael(N, testInteger):
			return 'carmichael'

	# If it isn't a composite and not a Carmichael number, it is a prime, probably
	return 'prime'

# O(n^3) time complexity
# O(n^2) space complexity
def mod_exp(x, y, N):
	# This check is O(n) complexity, at most (or less, depending on hardware)
	if y==0:
		return 1
	# Since we divide y in half every time (y is never bigger than N), this method is run log(N) times, where log is always base 2 (for all future log functions as well, assume that log is base 2). So the recursion runs n times
	# Each division in this line of code is a bit shift, so n time complexity for each division (although with hardware solution, bit shift could be constant time). The flooring is probably constant, since you can just ignore the remainder bits.
	# The space complexity for z in every loop is O(n), since the return statement must be smaller than or equal to N. Again, n is the number of bits in N.
	z = mod_exp(x,	math.floor(y/2), N)
	# This check is O(n^2) complexity
	if y % 2 == 0:
		# z^2 is O(n^2) complexity plus the modular division which is O(n^2). So, still a total of O(n^2)
		return (z**2) % N
	# z^2 is n^2 time complexity and plus a multiplication plus a modular division, so still a total of O(n^2)
	return (x * (z**2)) % N

# O(k^2) time complexity 
# Constant space complexity
def probability(k):
	# O(m) complexity, although it depends on the hardware (could be O(1) with hardware solution), where m is the number of bits in k
	if k == 0:
		return 0
	# One subtraction is O(m) and the division is O(m^2). However, the 2^k operation dominates everything else, because it is k^2 time complicated, which is far bigger than m^2. So O(k^2).
	return 1 - (1 / (2 ** k))

# O(n^4) time complexity 
# O(n^2) space complexity
def is_carmichael(N,a):
	# Set this variable so we can use it later in the calculations
	# O(n) for both space and time complexity
	halvedExponent = N - 1

	# While we can take the square root of the exponential multiplication or while we haven't failed or succeeded the test, keep executing the algorithm
	# This loop will run at most log(N) times, which is n times
	while(1):
		# We will divide N - 1 by 2 to have the effect of calculating the square root of the exponential multiplication. If that isn't possible, then the test was a success, this is probably not a Carmichael number
		# This check is O(n^2) time complexity
		if halvedExponent % 2 != 0:
			return 0
		# O(n^2) time complexity
		# O(n) space complexity. But we are just overwritting what we had written before, so there is no extra space allocated
		halvedExponent = halvedExponent / 2

		# Calculate (a^(N-1))^(1/2) i.e. Calculate the a to the power of (N - 1) / 2, then mod by N
		# O(n^4) time complexity, as calculated in the function
		# result has O(n) space complexity, since the return number can never be greater than N.
		# However, since the function calls the mod_exp function, the space complexity then must incorporate the space complexity of the mod_exp function. So, space complexity is of this line is O(n^2) 
		result = mod_exp(a, halvedExponent, N)

		# If the result is -1 (or the wraparound equivalent), then it is probably a prime number (it didn't fail this test)
		# O(n) time complexity
		if result == -1 or result - N == -1:
			return 0
		# If the result is not 1 nor -1, this is a Carmichael number
		# O(n) time complexity
		if result != 1:
			return 1

	# If the number passed all the tests, then it is not a Carmichael number (probably)
	return 0

